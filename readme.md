# DejaVu Sans Mono with added code ligatures

## Why ?

DejaVu Sans Mono has been my favorite coding font for a long time; but recently
(and with support [brought to emacs](https://melpa.org/#/ligature)), I became
fan of coding ligatures. Switching to [Cascadia
Code](https://github.com/microsoft/cascadia-code) was not very satisfying though
(that `f` really is ugly!), and DejaVu doesn't have built-in ligatures yet.

## How ?

Straight-forward using the very helpful
[Ligaturizer](https://github.com/ToxicFrog/Ligaturizer) script: I don't deserve
any credit for this, but thought putting the results here could save some time
if someone is interested.

The ligatures are just taken and adjusted from [Fira
Code](https://github.com/tonsky/FiraCode) ; I also took the liberty to import
the parens (`(` `)`) and tilde (`~`) for added curviness, and the asterisk (`*`)
for a more satisfying alignment.

## Installation

Here is what I use on Debian or equivalent:
- copy the .ttf files to ~/.fonts/ligadejavu
- run `fc-cache -vr`

## License

Fira Code is under [SIL Open Font License
1.1](https://github.com/tonsky/FiraCode/blob/master/LICENSE), which implies that
this must be under the same license.

DejaVu has [these terms](https://dejavu-fonts.github.io/License.html), which
seems globally equivalent (disclaimer: IANAL) and requires the copyright notices
present in the LICENSE file.
